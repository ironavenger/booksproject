Its a sample spring boot microservice to handle Backend API for a book store.
Designed using SpringBoot, JAVA 8, Junit5, Mockito, Hystrix and Spring JPA.

## Clone a repository
Step1 : open git
Step2: type this on git cmd : git clone https://ironavenger@bitbucket.org/ironavenger/booksproject.git
Step3: You are good to go, import in STS or any IDE and go on.
Step4: If in case required you need to provide DB, its address and password to run this application, in /books/src/main/resources/application.properties.

## Running in Docker container
Step1: please configure with following databse dependency to run application:
server.port=8081
spring.datasource.url = jdbc:postgresql://localhost:5432/bookdb  // in case i have used localhost, please configure your details
spring.datasource.username = postgres
spring.datasource.password = root
Step2: please use provided details to run a docker image in file booksproject/books/target/DockerFile
SOURCE CODE OF DOCKER FILE FOR YOUR CONVIENCE:
####################################
FROM java:8
EXPOSE 8081
ADD /books/target/books-0.0.1-SNAPSHOT.jar books-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "books-0.0.1-SNAPSHOT.jar"]
#####################################

In case of any issue running application please share logs with, contest.shubham@gmail.com will be happy to help! :smiley:
